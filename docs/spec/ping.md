# Request: Ping ("ping")

Connection keep-alive, "aliveness" and latency discovery.

If a node receives a PING message, it replies as quickly as possible with a [PONG](/pong.md) message with the provided *nonce*.


## Message Format

| Field | Length | Format | Description |  
|--|--|--|--|
|  nonce  | 8 bytes | unsigned 64 bit little endian integer | An arbitrary value provided to connect the ping message with the pong reply
