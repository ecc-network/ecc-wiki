# Response: Pong ("pong")

Connection keep-alive, "aliveness" and latency discovery.  This message is sent in response to a [PING](/ping.md) message.

## Message Format

| Field | Length | Format | Description |    
|--|--|--|--|  
| nonce | 8 bytes | unsigned 64 bit little endian integer  | The value provided by the [PING](/ping.md) message.
