*Provides a contiguous set of block headers*  

## HEADERS Message Format

|Size in bytes|Description|Data type|
|-------------|-----------|---------|
|1+           | Number of headers | varint|
| (80 + 2) * count  |[Block headers](/protocol/blockchain/block/block-header/), number of txs in the block, and block signature |Blockheader + varint|

No more than 2000 block headers may be sent at one time. Block headers in this array MUST be sequential, ordered by height and without range gaps.

The number of txs must be a varint encoded 0. The block signature must be an empty vector.
