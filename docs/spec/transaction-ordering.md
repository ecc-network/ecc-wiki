# Transaction Ordering

Transactions in a Block are inherently ordered by virtue of their inclusion in the [Merkle Tree](//merkle-tree.md).
Furthermore, the [Generation Transaction](/block.md#generation-transaction) is required to be the first Transaction in the Block.
After that, the order of any remaining Transactions in the block is dependent on when that Block was mined see [Topological Transaction Ordering](#legacy-transaction-ordering).

## Topological Transaction Ordering

With topological transaction ordering, Transactions after the Coinbase Transaction or Coinstake Transaction are required to abide by the following constraint: if a transaction B spends outputs created by transaction A, transaction B must appear after transaction A in the block.
Otherwise, transactions could appear in any order.
