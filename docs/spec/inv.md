*Notifies peers about the existence of some information (block or transaction)*
Based on selected services in the [VERSION](/version.md) message, INV messages may not be sent.

If a bloom filter has been sent to this node via [FILTERLOAD](/filterload.md), transaction INVs will only be sent if they match the bloom filter.

| compact int | 4 bytes | 32 bytes |... | 4 bytes | 32 bytes |
|----------|---------|----------|---|---------|----------|
|[vector](/vector.md) size N of|   type 1  |   hash 1  | | type N | hash N

NOTE: Since a block header is a relatively small data structure, and block propagation speed is an important network metric, a peer may send HEADER messages in place of INV messages when a block arrives.

##### Type
The type of the object that is available.

| Type | Value|
|------|------|
|   1  |  Transaction |
|   2  |  Block |
|   3  |  Filtered Block (partial block with merkle proof)

##### Hash
The [hash identifier](/glossary/hash__identifier.md) of the available object.
