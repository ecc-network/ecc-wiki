# Block

A block is a collection of transactions that have been committed to the blockchain as a bundle.
The transactions do not necessarily have to be related to one another and, generally, are only associated by virtue of when they were submitted to the network.

Every block contains a block header, one generation transaction, and zero or more additional transactions.

## Block Headers

Block are identified by the Scrypt hash of their header.
Since the block header contains the root of a [Merkle Tree](/merkle-tree.md) of the transactions in the block, this means the hash is ultimately dependent on both the meta-information about the block as well as the full contents of all of its component transactions.

For more information see [Block Header](/block-header.md).

## Generation Transaction

A generation transaction is the first transaction in a block that provides an opportunity for the block issuer to claim a reward for successfully generating a valid block. In a proof-of-work block this generation transaction is a coinbase transaction and the issuer of the block is called a miner. In a proof-of-stake block this generation transaction is a coinstake transaction and the issuer of the block is called a minter.

Transaction fees are often required by the network for transactions to be relayed across the network.
Satoshis provided as inputs to a transaction, but not consumed by its outputs, are collected by the generation transaction as implicit inputs.

### Coinbase Transaction

The coinbase transaction provides a mechanism for miners to:

 - Receive payment for mining.
 - Include a message, known as the "coinbase message," or simply "coinbase," within the block.

To accomplish this, the coinbase transaction is required to have a single input.
However, unlike other transactions, this input is not expected to provide the satoshis that appear in the outputs.
Instead, the satoshis that appear in the outputs of the coinbase transaction are collected from two places: the block reward and transactions fees.

The block reward is provided automatically to the block and represents a continually decreasing incentive for miners.

The block reward was 100,000,000,000 ecc-satoshis (100,000 ECC) for the first 86,400 blocks afterwhich proof-of-work blocks were not allowed by the network.

For more information, see [Mining](/mining.md).

### Coinstake Transaction

The coinstake transaction provides a mechanism for miners to:

 - Receive payment for minting.

To accomplish this, the coinstake transaction is required to have inputs spent by the minter that meet some minimum requirements.
// TODO : add information about requirements for an input to be used in the coinstake transaction.

The block reward is provided automatically to the block and represents a continually decreasing incentive for miners.
The block reward is a function of the amount of coins staked in the input.
// TODO : add information about the reward function for proof-of-stake blocks

For more information, see [Minting](/minting.md).
