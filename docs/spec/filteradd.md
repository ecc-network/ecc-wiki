# Request: Filter Add (“filteradd”)

Add an entry into the installed bloom filter.

## Message Format

| compact int | N bytes |
|-------------|---------|
|[vector](/vector.md) size N of| data
