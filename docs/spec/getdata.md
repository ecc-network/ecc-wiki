# Request: Get Data (“getdata”)

Requests information (generally previously announced via an INV) from a peer.

A GETDATA request is a [vector](/vector.md) of INV-formatted data.

## Message Format

| Field | Length | Format | Description |
|--|--|--|--|
| vector length N | variable | compact int | number of items |
| item 0 type | 4 bytes | unsigned little endian integer | type of the requested object |
| item 0 hash | 32 bytes | bytes | hash of the requested object |
| ... | | | |
| item N-1 type |
| item N-1 hash


### Type

The type of the desired object. See [INV](/inv.md) for specific values

### Hash
The [hash identifier](/hash__identifier.md) of the desired object.
