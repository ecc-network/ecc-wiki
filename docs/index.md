# Welcome to the ECC Wiki

## An Overview of ECC

ECC is a peer to peer electronic cash system heavily based on and influenced by Bitcoin as written by Satoshi Nakamoto [1]. The focus of the project is to build a robust set of network protocols around the electronic cash system that focus on privacy and security. This will in turn allow any developer to safely build products and services on top of the ECC network. To facilitate growth, it is important that the network allows any developer to build without restriction.

Building the network protocols around an electronic cash system allows for the dual use of consensus mechanisms in the network protocols and direct access to the cash system. This close relationship make it easy to access or store data in the blockchain.

The network has two different core components, routing and data transmission. The routing protocol is inspired and heavily influenced by Ad hoc On-Demand Distance Vector Routing (AODV). Instead of using IP address to identify a node, a randomly generated unique ID is used which helps to increase the amount of privacy a user has.

All of the network protocols use a standardised network packet template for data transmission. Although the template is standardised the contents of the payload can be in any format required by the program or service that is sending/receiving the data. When encrypted, this packet only reveals information about its destination, not its source. Verification of the source is not implemented at the network level. It is recommended for developers that need this feature to include a signature as part of the payload that can be used to verify the sender.

References  
    [1] Nakamoto, S. (2008, October 31). Bitcoin: A Peer-to-Peer Electronic Cash System. Retrieved from https://bitcoin.org/bitcoin.pdf
